This repository contains Swarm testable testcase generators that are compatible with Atte's [NodeFuzz](https://github.com/attekett/NodeFuzz) browser fuzzing framework. 

In order to run these testcases one needs to drop them in the `NodeFuzz/moudles` directory




There are currently 3 Non-default NodeFuzz test case generators:

HTML5 DataList

HTML5 SVG

HTML5 WebAudio