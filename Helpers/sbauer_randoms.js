generateUInt = function (max){
   return Math.floor(Math.random()*max);
}

generateFloatLT1 = function(){
    return Math.random();
}

generateFloatLT1_neg = function() {
   var num = generateFloatLT1();
   return (generateFiftyProbability()) ? (-1 * num) : num;
}

generateInt = function(max) {
    var num = generateUInt(max);
    return (generateFiftyProbability()) ? (-1 * num) : num;
}

generateXYCoord = function(xmax,ymax) {
    var ret_str = generateUInt(xmax);
    ret_str += ',';
    ret_str += generateUInt(ymax);
    return ret_str;
}

generateRGB = function() {
    return 'rgb(' + generateUInt(256) + ',' + generateUInt(256)
        + ',' + generateUInt(256) + ')';
}

generateUIntOrFloatLT1 = function(max) {
    var num = (generateFiftyProbability()) ? 
                generateFloatLT1() : generateUInt(max);
    
    return num;
}

generateFiftyProbability = function() {
    var num = generateUInt(3);
    return ((num % 2) == 0);
}
    
generateThirdProbablity = function () {
    var num = generateUInt(4);
    return ((num % 3) == 0);
}

generateThreeFourthProbability = function() {
    var num = generateUInt(5);
    return ((num % 4) != 0);
}

generateOneFourthProbability = function() {
    var num = generateUInt(5);
    return ((num % 4) == 0);
}


/* http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
generateRandomPowerOfTwo = function(max){
    var v = generateUInt(max);
    
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    
    return v;
} 

generateDouble = function(max){
    return Math.random * max;
}


/* Generate and Integer or float<1 possibly negative */
generateIntOrFloatLT1_neg = function(max) {
    var num = generateUIntOrFloatLT1(max);
    num *= (generateFiftyProbability()) ? -1 : 1;
    
    return num;
}

generateRandomString = function(max) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var length = max;

    for(var i = 0; i < length ; i++){
        if(i == 0){
            text += possible.charAt(Math.floor(Math.random() * (possible.length-10)));
        }
        else{
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    return text;
}

generateTrueOrFalse = function() {
    var tf = 
        ['true', 'false'];
    return shuffle(tf)[0];
}

generateBoolean() = function() {
    return generateTrueOrFalse();
}



shuffle = function(v){
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
    return v;
};
