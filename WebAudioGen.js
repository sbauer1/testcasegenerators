 
require('./Helpers/sbauer_randoms.js');


var DEBUG_OUTPUT = false;

var SWARM = false;


/* http://www.w3.org/TR/webaudio/#API-section */
var context_functions = [
    ['createBuffer', createBufferVarGen],
    ['decodeAudioData', createdecodeAudioDataVarGen],
    ['createBufferSource', createBufferSourceVarGen],
    ['createMediaElementSource', createMediaElementSourceVarGen],
    ['createMediaStreamSource', createMediaStreamSourceVarGen],
    ['createMediaStreamDestination', createMediaStreamDestinationVarGen],
    ['createScriptProcessor', createScriptProcessorVarGen],
    ['createAnalyser', createAnalyserVarGen],
    ['createGain', createGainVarGen],
    ['createDelay', createDelayVarGen],
    ['createBiquadFilter', createBiquadFilterVarGen],
    ['createWaveShaper', createWaveShaperVarGen],
    ['createPanner', createPannerVarGen],
    ['createConvolver', createConvolverVarGen],
    ['createChannelSplitter', createChannelSplitterVarGen],
    ['createChannelMerger',  createChannelMergerVarGen],
    ['createDynamicsCompressor', createDynamicsCompressorVarGen],
    ['createOscillator', createOscillatorVarGen],
    ['createPeriodicWave', createPeriodicWaveVarGen]
];

/* Available calls for Audio Buffer Source Node 
   http://www.w3.org/TR/webaudio/#AudioBufferSourceNode-section
*/

/*var Absn = [
    ['start', //generateABSNStart],
    ['stop', generateABSNStop]
];*/
   

/* available functions for the AudioNode Interface
   http://www.w3.org/TR/webaudio/#AudioNode-section/
*/
var Audio_Node_Fn = [
    "conect",
    "connect1",
    "disconnect"
];


var nodes_with_params = [
    ['GainNode', gainParams],
    ['DelayNode', delayParams],
    ['ScriptProcessorNode', scriptProcessParams],
    ['PannerNode', pannerParams],
    ['ConvolverNode', convolveParams],
    ['AnalyserNode', analyserParams],
    //['DynamicsCompressorNode', dynParams] TODO later only RO params
    //['BiquadFilterNode', bifilParams], TODO later only RO params -- has fn's
    ['WaveShaperNode', waveParams],
    ['OscillatorNode', oscilParams],
    
    
];

var oscilParams = [
    ['Attribute', 'type', 'OscillatorType', 'RW'],
    ['Attribute', 'frequency', 'AudioParam', 'RO'],
    ['Attribute', 'detune', 'AudioParam', 'RO'],
];

var oscilFunctions = [
    'start',
    'stop',
    'setPeriodicWave'
];

var waveParams = [
    ['Attribute', 'curve', 'Float32Array', 'RW'],
    ['Attribute', 'oversample', 'OverSampleType', 'RW']
];

var bifilFunctions = [
    'getFrquencyResponse'
];


var analyserFunctions = [
    'getFloatFrequencyData',
    'getByteFrequencyData',
    'getByteTimeDomainData'
];

var analyserParams = [
    ['Attribute', 'fftSize', 'ulong', 'RW'],
    ['Attribute', 'frequencyBinCount', 'ulong', 'RO'],
    ['Attribute', 'minDecibles', 'double', 'RW'],
    ['Attribute', 'maxDecibles', 'double', 'RW'],
    ['Attribute', 'smoothingTimeConstant', 'double', 'RW']
];

var convolveParams = [
    ['Attribute', 'buffer', 'AudioBuffer', 'RW'],
    ['Attribute', 'normalize', 'boolean', 'RW']
];

var pannerParams = [
    ['Attribute', 'panningModel', 'PanningModelType', 'RW'],
    ['Attribute', 'distanceModel', 'DistanceModelType', 'RW'],
    ['Attribute', 'refDistance', 'double', 'RW'],
    ['Attribute', 'maxDistance', 'double', 'RW'],
    ['Attribute', 'rolloffFactor', 'double', 'RW'],
    ['Attribute', 'coneInnerAngle', 'double', 'RW'],
    ['Attribute', 'coneOutterAngle', 'double', 'RW'],
    ['Attribute', 'coneOuterGain', 'double', 'RW'],
];

var pannerFunctions = [
    'setPosition',
    'setOrientation',
    'setVelocity'
];

var scriptProcessParams = [
    ['Attribute', 'bufferSize', 'long', 'RO'],
    ['Attribute', 'onaudioprocess', 'EventHandler', 'RW']
];

var delayParams = [
    ['Attribute', 'delayTime', 'AudioParam', 'RO']
];

var gainParams = [
    ['Attribute', 'gain', 'AudioParam',  'RO']
];


var type_mapper = [
    ['createBufferSource', 'AudioBufferSourceNode', undefined],
    ['createMediaElementSource', 'MediaElementAudioSourceNode', Audio_Node_fn],
    ['createMediaStreamSource', 'MediaStreamAudioSourceNode', undefined],
    ['createMediaStreamDestination', 'MediaStreamAudioDestinationNode', undefined],
    ['createScriptProcessor', 'ScriptProcessorNode', undefined],
    ['createAnalyser', 'AnalyserNode', undefined],
    ['createGain', 'GainNode', undefined],
    ['createDelay', 'DelayNode', undefined],
    ['createBiquadFilter', 'BiquadFilterNode', undefined],
    ['createWaveShaper', 'WaveShaperNode', undefined],
    ['createConvolver', 'ConvolverNode', undefined],
    ['createChannelSplitter', 'ChannelSplitterNode', undefined],
    ['createChanelMerger', 'ChannelMergerNode', undefined],
    ['createDynamicsCompressor', 'DynamicsCompressorNode', undefined],
    ['createOscillator', 'OscillatorNode', undefined],
    ['createPeriodicWave', 'PeriodicWave', undefined] 
];

    
var avail_vars_types;   // will look like 'string varname', context_type
var browserGlobals = 'var context;\n var audiobuffer;\n';
var globalHTML = "";

function generateTestCase(){
    console.log("Call");
    global.gc();
    FuzzedFunction =  'function FuzzedFunction(){\n';
    avail_vars_types = new Array();
    globalHTML = '<html> <title>WEB_AUDIO_GEN</title><body><h1>WEB_AUDIO_GEN</h1>';
    var js = '<script> \n window.onload=init;';
    
    
    js += init.toString() + '\n';
    js += setupAudioContext.toString() + '\n';
    js += loadSound.toString() +'\n';
    js += 'function generateUInt(max) { return Math.floor(Math.random()*max); }\n';
   
    var loops = generateUInt(65535);
    console.log(loops);
    for(var x = 0; x < loops; x++){
        generateRandomSetOfVars();
    }

    ComileFuzzedFunction();

    js += FuzzedFunction + '}\n';//.toString();
    globalHTML += js + '\n' + browserGlobals;
    

    globalHTML += '\n</script></html>';
    if(DEBUG_OUTPUT)
        console.log(globalHTML);
    console.log("return");
    return globalHTML;
}


/*
  This function is the brains of the operation.
  This function randomly iterates over the available
  generated vars from generateRandomSetOfVars()
  which are stored in avail_vars_types array.
  The code will attempt to tie together Nodes that 
  are compatable, while setting up fuzzed values for
  properties. If no compatable node exists for tying 
  together, it will skip or use a non-compatable node.
 
  TODO: How can I do code reuse with the vargen funciton??

 */

function CompileFuzzedFunction(){
    var num_to_tie = generateUInt(avail_vars_types.length);
    var new_vars = splice(0,num_to_tie);
    for(var i = 0; i < num_to_tie; i++){
        switch(new_vars[i][1]){
        case 'createBufferSource':
            /*ignore for now*/
            break;
        case 'createMediaElementSource':
            /* One output, zero inputs */
            /* can connect() */
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            break;
        case 'createMediaStreamSource':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            break;
        case 'createMediaStreamDestination':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
             break;
        case 'createScriptProcessor':
            /* TODO Has a RW event handler */

            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            break;
        case 'createAnalyser':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doAttrsForArrayType(new_vars[i][0],analyserParams);
            doAnalyserFunctions(new_vars[i][0]);
            break;
        case 'createGain':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            break;
        case 'createDelay':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            break;
        case 'createBiquadFilter':
            if(generateFiftyProbability()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doBiquadFilterFunctions(new_vars[i][0]);
            break;
        case 'createWaveShaper':
            if(generateFiftyProbability()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doAttrsForArrayType(new_vars[i][0], waveParams);
            break;
        case 'createPanner':
            if(generateFiftyProbability()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doAttrsForArrayType(new_vars[i][0],pannerParams);
            doPannerFunctions(new_vars[i][0]);
            break;
        case 'createConvolver':
            if(generateFiftyProbablity()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doAttrsForArrayType(new_vars[i][0],convolveParams);
            /* no functions to tie */

            break;
        case 'createChannelSplitter':
            
            break;
        case 'createChannelMerger':
            
            break;
        case 'createDynamicsCompressor':
            
            break;
        case 'createOscillator':
            if(generateFiftyProbability()){
                generateGenericAudioNode(new_vars[i][0], new_vars);
            }
            doAttrsForArrayType(new_vars[i][0],oscilParams);
            doOscillatorFunctions(new_vars[i][0]);
            break;
        case 'createPeriodicWave':
            
            break;
        default:
            console.log('Compile Function failed! With ' + context_functions[i][0]);
            GARBAGE[0];

        }
    }
        
        
        

}
    




function generateRandomSetOfVars(){
    
    shuffle(context_functions);
    for(var i=0; i < context_functions.length; i++){
        switch(context_functions[i][0]){
        case 'createBuffer':
            createBufferVarGen();
            break;
        case 'decodeAudioData':
            break;
        case 'createBufferSource':
            createBufferSourceVarGen();
            break;x
        case 'createMediaElementSource':
            createMediaElementSourceVarGen();
            break;
        case 'createMediaStreamSource':
            createMediaStreamSourceVarGen();
            break;
        case 'createMediaStreamDestination':
            createMediaStreamDestinationVarGen();
             break;
        case 'createScriptProcessor':
            createScriptProcessorVarGen();
            break;
        case 'createAnalyser':
            createAnalyserVarGen();
            break;
        case 'createGain':
            createGainVarGen();
            break;
        case 'createDelay':
            createDelayVarGen();
            break;
        case 'createBiquadFilter':
            createBiquadFilterVarGen();
            break;
        case 'createWaveShaper':
            createWaveShaperVarGen();
            break;
        case 'createPanner':
            createPannerVarGen();
            break;
        case 'createConvolver':
            createConvolverVarGen();
            break;
        case 'createChannelSplitter':
            createChannelSplitterVarGen();
            break;
        case 'createChannelMerger':
            createChannelMergerVarGen();
            break;
        case 'createDynamicsCompressor':
            createDynamicsCompressorVarGen();
            break;
        case 'createOscillator':
            createOscillatorVarGen();
            break;
        case 'createPeriodicWave':
            createPeriodicWaveVarGen();
            break;
        default:
            console.log('VARGEN Function failed! With ' + context_functions[i][0]);
            GARBAGE[0];
            break;
        }
    }
}



/* TODO , connect can take optional args randomly put args in */

function generateGenericAudioNode(input_var, avail_node_array){
    var select_fn = generateUInt(Audio_Node_Fn.length);
    switch(Audio_Node_fn[select_fn]){
        /*NODE CONNECT*/
    case 'connect':
        var name = findRandomNodeToConnect(avail_node_array, "AudioNode");
        FuzzedFunction += input_var + '.connect('+name+');\n';
        break;
        /* PARAM CONNECT */
    case 'connect1':
        break;
    case 'disconnect':
        FuzzedFunction += input_var + '.disconnect();\n';
        break;
    }
}

/*
  Attempts to find a good node for "node_type"
  ie if createWaveShaper only can "connect" with 
  BiquadFilter nodes then we will look for that node,
  if it can connect to anything we randomly select 
  a node if we cannot find a node we will iterate over
  the entire node array, not the subset we created above.
  If no such node exists, we will randomly assign a node 
  to connect to. *including* the current node. Ie 
  node.connect(node); could occur, because, why not.
*/

function findRandomNodeToConnect(node_array, node_type){
/*    for(var i = 0; i < node_array.length; i++){
        if(compatable(node_array[i][1], node_type)){
            return node_array[i][0]; //return variable name;
        }
    }
    for(var i = 0; i < avail_vars_types.length; i++){
        if(compatable(avail_vars_types[i][1], node_type)){
            return avail_vars_types[i][0];
        }
    }*/
    if(node_array.length > 0){
        return node_array[generateUInt(node_array.length)][0];
    }
    if(avail_vars_types.length > 0){
        return node_array[generateUInt(avail_vars_types.length)][0];
    }
    console.log("THERE WERE NO NODES TO CONNECT, BAD SWARM? OR ERROR DEBUG!");
    return ""; 
}


/* TODO TURN THIS SHIT INTO A BST, CONSTANTLY DOING N^2 == DUMB */
function compatable(node_type, compatable_with_this){
    return true;
    
/* TODO, Do certain nodes prefer other nodes? figure that out and see if its easy
   to code for now all nodes are compatable with each other */


/*
    for(var i = 0; i < type_mapper.length; i++){
        if(type_mapper[i][0] == node_type
    }
*/
}


/*
enum OscillatorType {
  "sine",
  "square",
  "sawtooth",
  "triangle",
  "custom"
}; */

function generateOscilType(){
  var osctypes = [
  "sine",
  "square",
  "sawtooth",
  "triangle",
  "custom"
];
    return osctypes[generateUInt(osctypes.length)];
}

function genFuzzedValueForAttr(type){
    var dMax = 65535;

    switch(type){
    case "double":
        return generateDouble(dMax);
    case "boolean":
        return generateBoolean();
    case "OscillatorType":
        return generateOscilType();
    case "Float32Array":
        break;
    case "OverSampleType":
        break;
    case "ulong":
        break;
    case "AudioBuffer":
        break;
    case "PanningModelType":
        break;
    case "DistanceModelType":
        break;
    case "EventHandler":
        break;
    

    default:
        console.log("NOT YET IMPLEMENTED, " + type);
        GARBAGE[0];
        break;
    }

}


function doAttrsForArrayType(var_name, array_of_attributes){
    var amount = array_of_attributes.length;
    if(!SWARM){
        amount = generateUInt(amount);
        shuffle(array_of_attributes);
    }
    for(var i = 0; i < amount; i++){
        if(array_of_attributes[i][3] == 'RW'){
            FuzzedFunction += var_name + '.' + array_of_attributes[i][1] + ' = ' +
                genFuzzedValueForAttr(array_of_attributes[i][3]) + ';\n';
        }
    }

}


function doOscillatorFunctions(var_name){
    var amount = oscilFunctions.length;
    if(!SWARM){
        amount = generateUInt(amount);
        shuffle(oscilFunctions);
    }
    for(var i = 0; i < amount; i++){
        FuzzedFunction += var_name + '.' + oscilFunctions[i] + '(';
        switch(oscilFunctions[i]){
        case 'stop':
        case 'start':
            FuzzedFunction += generateDouble(65535) + ');\n';
            break;
        case 'setPeriodicWave':
            FuzzedFunction += genFuzzedValueForAttr('PeriodicWave') + ');\n';
            break;
        }
    }
}

function doBiquadFilterFunctions(var_name){
    var fuzzed_array_types = [
        'Uint8Array',
        'Float32Array',
        'Float16Array',
        'Int16Array',
        'Int32Array'
    ];


    //No need for swarm check as there is only one function that can be called. 
    //if swarm removed it bifil length will be 0.
    if(bifilFunctions.length > 0){
        var use_random = generateOneFourthProbability();
        //Using the true array types
        var arr_n1 = generateRandomString(15);
        var arr_n2 = generateRandomString(15);
        var arr_n3 = generateRandomString(15);
        
        
        if(!use_random){
            if(generateoneFourthProbability()){
                FuzzedFunction += var_name + '.getFrequencyRespones(new Float32Array(' +
                    generateUInt(65536)+'), new Float32Array(' + generateUInt(65535) + '), '+
                    'new Float32Array(' + generateUInt(65536) + '));\n';
            }
            else{
                FuzzedFunction += 'var ' + arr_n1 + '= new Float32Array(' + 
                    generateUInt(65536) + ');\n'
                FuzzedFunction += 'var ' + arr_n2 + '= new Float32Array(' + 
                    generateUInt(65536) + ');\n';
                FuzzedFunction += 'var ' + arr_n3 + '= new Float32Array(' + 
                    generateUInt(65536) + ');\n';
                FuzzedFunction += var_name + '.getFrequencyRespones(' + arr_n1 +
                    ', '+ arr_n2 + ', ' + arr_n3 + ' );\n';
            }
            return;
        }
        //Slap random array types into the function calls. same way above, with new directly
        //or referenced via variable.
        
        var ind1 = fuzzed_array_types[generateUInt(fuzzed_array_types.length)];
        var ind2 = fuzzed_array_types[generateUInt(fuzzed_array_types.length)];
        var ind3 = fuzzed_array_types[generateUInt(fuzzed_array_types.length)];

        if(generateoneFourthProbability()){
            FuzzedFunction += var_name + '.getFrequencyRespones(new '
                +ind1   +'(' + generateUInt(65536)+'), ' +
                'new ' + ind2 + '(' + generateUInt(65535) + '), '+
                'new ' + ind3 + '(' + generateUInt(65536) + '));\n';
        }
        else{
            FuzzedFunction += 'var ' + arr_n1 + '= new ' + ind1+ '(' + 
                generateUInt(65536) + ');\n'
            FuzzedFunction += 'var ' + arr_n2 + '= new ' + ind2+ '(' + 
                generateUInt(65536) + ');\n';
            FuzzedFunction += 'var ' + arr_n3 + '= new ' + ind3+ '(' + 
                generateUInt(65536) + ');\n';
            FuzzedFunction += var_name + '.getFrequencyRespones(' + arr_n1 +
                ', '+ arr_n2 + ', ' + arr_n3 + ' );\n';
        }
    }
}


function doAnalyserFunction(var_name){

    var fuzzed_array_types = [
        'Uint8Array',
        'Float32Array',
        'Float16Array',
        'Int16Array',
        'Int32Array'
    ];
    var amount = analyserFunctions.length;
    if(!SWARM){
        amount = generateUInt(amount);
        shuffle(analyserFunctions);
    }
    for(var i = 0; i < amount; i++){

        var array_name = generateRandomString(15);
        var fuzzed_array_index = generateUInt(fuzzed_array_types.length);

        /* SET UP var gen_name = new ArrayType(); */
        FuzzedFunction += 'var ' + array_name + ' =  new ' + 
            fuzzed_array_types[fuzzed_array_index] + '(' + generateUInt(65535) +
            ');\n'

        /*Setup call with Fuzzed Type */
        FuzzedFunction += var_name + '.' + analyserFunctions[i] + '(' + 
            array_name + ');\n';
     }
}


function doPannerFunctions(var_name){
    var dMax = 65535;
    if(SWARM){
        for(var i = 0; i < pannerFunctions.length; i++){
            FuzzedFunction += var_name + '.' + pannerFunctions[i] + '(' + generateDouble(dMax) +
                ', ' + generateDouble(dMax) + ', ' + generateDouble(dMax) + ');\n';
        }
    }
    else{
        FuzzedFunction += var_name + '.' + pannerFunctions[generateUInt(pannerFunctions.length)]
            +  '(' + generateDouble(dMax) + ', ' + generateDouble(dMax) + ', ' + 
            generateDouble(dMax) + ');\n';
    }
}


/*
    Creates a PeriodicWave representing a waveform containing arbitrary harmonic 
    content. The real and imag parameters must be of type Float32Array of equal 
    lengths greater than zero and less than or equal to 4096 or an INDEX_SIZE_ERR 
    exception MUST be thrown. These parameters specify the Fourier coefficients of 
    a Fourier series representing the partials of a periodic waveform. The created 
    PeriodicWave will be used with an OscillatorNode and will represent a 
    normalized time-domain waveform having maximum absolute peak value of 1. 
    Another way of saying this is that the generated waveform of an OscillatorNode 
    will have maximum peak value at 0dBFS. Conveniently, this corresponds to the 
    full-range of the signal values used by the Web Audio API. Because the 
    PeriodicWave will be normalized on creation, the real and imag parameters represent 
    relative values.

    The real parameter represents an array of cosine terms (traditionally the A terms). 
    In audio terminology, the first element (index 0) is the DC-offset of the periodic 
    waveform and is usually set to zero. The second element (index 1) represents the 
    fundamental frequency. The third element represents the first overtone, and so on.

    The imag parameter represents an array of sine terms (traditionally the B terms). 
    The first element (index 0) should be set to zero (and will be ignored) since this 
    term does not exist in the Fourier series. The second element (index 1) represents 
    the fundamental frequency. The third element represents the first overtone, and so on.

*/

function createPeriodicWaveVarGen(){
    var size_varname = generateRandomString(15);
    var real_varname = generateRandomString(15);
    var imag_varname = generateRandomString(15);
    var pwave_var = generateRandomString(15);

    FuzzedFunction +=  'var ' + size_varname + ' = generateUInt(4096);\n';
    FuzzedFunction +=  'var ' + real_varname + ' = new Float32Array(' + size_varname + ');\n';
    FuzzedFunction +=  'var ' + imag_varname + ' = new Float32Array(' + size_varname + ');\n';

    FuzzedFunction += 'for(var i = 0; i < ' + size_varname + '; i++){\n' +  
        real_varname + '[i] = generateUInt(4294967296);\n' + 
        imag_varname + '[i] = generateUInt(4294967296);\n}\n';

    FuzzedFunction += 'var ' + pwave_var + ' = generatePeriodicWave(' + 
        real_varname + ', ' + imag_varname + ');\n';
}

function createOscillatorVarGen(){
    var osVar = generateRandomString(15);
    avail_vars_types.push([osVar, 'createDynamicsCompressor']);

    FuzzedFunction += 'var ' + osVar + ' = context.createOscillator();\n'; 
}

function createDynamicsCompressorVarGen(){
    var dynComp = generateRandomString(15);
    avail_vars_types.push([dynComp, 'createDynamicsCompressor']);

    FuzzedFunction += 'var ' + dynComp + ' = context.createDynamicsCompressor();\n'; 
}


function createChannelMergerVarGen(){
    var chanVar = generateRandomString(15);
    avail_vars_types.push([chanVar, 'createChannelMerger']);

    var numChans = generateUInt(1025);
    
    if(generateFiftyProbability()){
        FuzzedFunction += 'var ' + chanVar + ' = context.createChannelMerger(' +
            numChans + ');\n';
    }
    else{
        FuzzedFunction += 'var ' + chanVar + ' = context.createChannelMerger();\n';
    }
}

function createChannelSplitterVarGen(){
    var chanVar = generateRandomString(15);
    avail_vars_types.push([chanVar, 'createChannelSplitter']);

    var numChans = generateUInt(1025);

    if(generateFiftyProbability()){
        FuzzedFunction += 'var ' + chanVar + ' = context.createChannelSplitter(' +
            numChans + ');\n';
    }
    else{
        FuzzedFunction += 'var ' + chanVar + ' = context.createChannelSplitter();\n';
    }
}

function createConvolverVarGen(){
    var conVar = generateRandomString(15);
    avail_vars_types.push([conVar, 'createConvolver']);
    
    FuzzedFunction += 'var ' + conVar + ' = context.createcConvolver();\n';
}

function createPannerVarGen(){
    var panVar = generateRandomString(15);
    avail_vars_types.push([panVar, 'createPanner']);

    FuzzedFunction += 'var ' + panVar + ' = context.createPanner();\n';
}

function createWaveShaperVarGen(){
    var waVar = generateRandomString(15);
    avail_vars_types.push([waVar, 'createWaveShaper']);
    
    FuzzedFunction += 'var ' + waVar + ' = context.createWaveShaper();\n';
}

function createBiquadFilterVarGen(){
    var biVar = generateRandomString(15);
    avail_vars_types.push([biVar, 'createBiquadFilter']);

    FuzzedFunction += 'var ' + biVar + ' = context.createBiquadFilter();\n';

}
    
/* max delay of 3 min or 180 sec */
function createDelayVarGen(){
    var delayVar = generateRandomString(15);
    var Double_delay = (Math.random() * 180);
    
    avail_vars_types.push([delayVar, 'createDelay']);
    FuzzedFunction += 'var  ' + delayVar + ' = context.createDelay(' + Double_delay + ');\n';

}

function createGainVarGen(){
    var gVar = generateRandomString(15);
    avail_vars_types.push([gVar, 'createGain']);

    FuzzedFunction += 'var ' + gVar + ' = context.createGain();\n';
}

function createAnalyserVarGen(){
    var aVar = generateRandomString(15);
    avail_vars_types.push([aVar, 'createAnalyser']);

    FuzzedFunction += 'var ' + aVar + ' = context.createAnalyser();\n';
}

/*

bufferSize
    The buffer size in units of sample-frames. If specified, the bufferSize must be
    one of the following values: 256, 512, 1024, 2048, 4096, 8192, 16384. If it's not 
    passed in, or if the value is 0, then the implementation will choose the best buffer 
    size for the given environment, which will be constant power of 2 throughout the 
    lifetime of the node.
    
    This value controls how frequently the audioprocess event is dispatched and how many 
    sample-frames need to be processed each call. Lower values for bufferSize will result 
    in a lower (better) latency. Higher values will be necessary to avoid audio breakup
    and glitches. It is recommended for authors to not specify this buffer size and allow 
    the implementation to pick a good buffer size to balance between latency and audio quality.

numberOfInputChannels
    Integer specifying the number of channels for this node's input, defaults to 2. Values 
    of up to 32 are supported.
    
numberOfOutputChannels
    Integer specifying the number of channels for this node's output, defaults to 2. Values 
    of up to 32 are supported. 

    Important: Webkit currently (version 31) requires that a valid bufferSize be passed when 
    calling this method.
*/

function createScriptProcessorVarGen(){
    var bufferSize = (generateOneFourthProbability() ? 
                      generateUInt(65536) : generateRandomPowerOfTwo(generateUInt(262145)));
    var inputChans = (generateOneFourthProbability() ? generateUInt(1024) : generateUInt(33));
    var outputChans =  (generateOneFourthProbability() ? generateUInt(1024) : generateUInt(33));
    var spVar = generateRandomString(15);

    FuzzedFunction += 'var ' + spVar + ' = context.createScriptProcessor(' + bufferSize +
        ', ' + inputChans + ', ' + outputChans + ' );\n';

}

function createMediaStreamDestinationVarGen(){
    var msdVar = generateRandomString(15);
    avail_vars_types.push([msdVar, 'createMediaStreamDestination']);

    FuzzedFunction += 'var ' + msdVar + ' = context.createMediaStreamDestination();\n';
}

function createMediaStreamSourceVarGen(){
    var micVar = generateRandomString(15);

    FuzzedFunction += '\
var ' + micVar + ';\n\
navigator.getUserMedia({ audio: true }, function(stream){\
     ' + micVar + ' = stream;\n\
}, function(){ console.log("Error getting Microphone stream"); });\n'

    var micStreamSource = generateRandomString(15);
    
    avail_vars_types.push([micVar, 'getUserMedia']);
    avail_vars_types.push([micStreamSource, 'createMedaStreamSource']);

    FuzzedFunction += 'var ' + micStreamSource + ' = context.createMediaStreamSource(' +
        micVar + ');\n';

}

function createMediaElementSourceVarGen(){
    var varname = generateRandomString(15);
    avail_vars_types.push([varname, 'createMediaElementSource']);
    
    var id = generateRandomString(15);
    globalHTML += '\n<audio id="' + id + '" ></audio>';

    
    FuzzedFunction += 'var ' + varname + ' = context.createMediaElementSource(document.querySelector("' + id + '"));\n';
    
}


function createBufferSourceVarGen(){
    var varname = generateRandomString(15);
    avail_vars_types.push([varname, 'createBufferSource']);
    
    FuzzedFunction += 'var ' + varname + ' = context.createBufferSource();\n';    
}


/*
  The createBuffer method

  Creates an AudioBuffer of the given size. The audio data in the buffer will 
  be zero-initialized (silent). An NOT_SUPPORTED_ERR exception MUST be thrown if 
  the numberOfChannels or sampleRate are out-of-bounds, or if length is 0.
  
  The numberOfChannels parameter determines how many channels the buffer will have.
  An implementation must support at least 32 channels.
  
  The length parameter determines the size of the buffer in sample-frames.
  
  The sampleRate parameter describes the sample-rate of the linear PCM audio data in 
  the buffer in sample-frames per second. An implementation must support sample-rates 
  in at least the range 22050 to 96000.

*/

function createBufferVarGen(){
    var varname = generateRandomString(15);
    avail_vars_types.push([varname, 'createBuffer']);
    
    FuzzedFunction += 'var ' + varname + ' = context.createBuffer(';
    /* numberOfChannels 1/4 chance of < 1024 and 3/4 chance of <=32 */
    FuzzedFunction += (generateThreeFourthProbability() ? generateUInt(1024) : generateUInt(33));
    FuzzedFunction += ',';
    
    FuzzedFunction += generateUInt(65535);
    FuzzedFunction += ',';
    
    var sampleRate = generateUInt(96001);
    if(sampleRate < 22050){
        sampleRate += 22050;
    }
    FuzzedFunction += (generateThreeFourthProbability() ? generateUInt(10000) : sampleRate);
    FuzzedFunction += ');\n';

}


function createdecodeAudioDataVarGen(){
    /*NO OP FOR NOW*/
}



function debugPrintFn(fn){
    console.log(fn.toString());
}

/* EMPTY DEFAULT FUNCTION WILL ADD TO IT AS WE COMPILE OUR FUZZED JS CALLS */
var FuzzedFunction; 






/* UNCTIONS BELOW ARE TO BE DIRECTLY APPENDED TO THE JS, ABOVE ARE HELPER FUNCTIONS 
   FOR FUZZEDFUNCTION() GENERATION 
*/

function init(){
    setupAudioContext();
    var port = document.URL.substring(18,24);
    loadSound('http://127.0.0.1:'+port+'/u.mp3');//'http://eng.utah.edu/~sbauer/u.mp3');//generateRandomFile());
    FuzzedFunction();
}

function loadSound(url){
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = 'arraybuffer';

  // Decode asynchronously
  request.onload = function() {
    context.decodeAudioData(request.response, function(buffer) {
      audiobuffer = buffer;
    }, function(){ alert('FAILED ' + url); });
  }
  request.send();
}

function setupAudioContext() {
    try {
        window.AudioContext = window.AudioContext||window.webkitAudioContext;
        context = new AudioContext();
    }
    catch(e) {
        alert('Web Audio API is not supported in this browser');
    }
}



function stripFunction(fn){
    return fn.substring(fn.indexOf('{')+1, fn.length-2);
}



//
//Necessary exports
// 
module.exports={
 //
 //fuzz: 
 //Required synchronous function.
 //Must return a test case either as a Buffer or as a String	
 //
 fuzz:function(){
        return generateTestCase()
 },
 //
 //init:
 //Optional function
 //Doesn't require a return-value.
 //Can be used to do initialisations required by the fuzz-module.
 init:function(){
		config.type='text/html'
		config.tagtype='html'
		config.clientFile=config.reBuildClientFile()
 }
}
