require('./Helpers/sbauer_randoms.js')

/*
  Attempting to create fuzzed version of:
  <svg >
*/

/*
  TODO:
  add transformations:
  add gradients 
  text?
*/

var OUTPUT = false;

var DEBUG_ONE = false;

var UINT_MAX =  128;//4294967295;

var styles = [
    ['fill', 'rgb'],
    ['stroke', 'rgb'],
    ['stoke-width', 'uint'],
    ['fill-opacity', 'float_lt1'],  //float < 1.0
    ['stroke-opacity', 'float_lt1'],
    ['font-size', 'uint_px'],
    ['fill-rule', 'fill-string'],
    ['stroke-linecap', 'lcap-string'],
    ['stroke-dasharray', 'Nxy-coord'],
    ['stroke-linejoin', 'ljoin-string']
];

var path_modifiers = [
    ['L', 'lineto'],
    ['H', 'horizontal lineto'],
    ['V', 'vertical lineto'],
    ['C', 'curveto'],
    ['S', 'smooth curveto'],
    ['Q', 'quadratic Bézier curve'],
    ['T', 'smooth quadratic Bézier curveto'],
    ['A', 'elliptical Arc']
];
  
var objects = [
    ['Circle', Circle],
    ['Rectangle', Rectangle],
    ['Ellipse', Ellipse],
    ['Line', Line],
    ['Polygon', Polygon],
    ['Polyline', Polyline],
    ['Path', Path]
];

var filters = [  
   'feBlend', // - filter for combining images
    'feColorMatrix',// - filter for color transforms
    'feComponentTransfer',
    'feComposite',
    'feConvolveMatrix',
    'feDiffuseLighting',
    'feDisplacementMap', 
    'feFlood',
    'feGaussianBlur', 
    //'feImage',  //Do this later -- kinda hard i guess?
    'feMerge',
    'feMorphology', 
    'feOffset',  //- filter for drop shadows
    'feSpecularLighting',
     'feTile', 
    'feTurbulence',
    'feDistantLight',// - filter for lighting
    'fePointLight', //- filter for lighting
    'feSpotLight' //- filter for lighting*/
];

var feColorMatrixTypes = [ 
    ['matrix', 'matrix'],
    ['saturate', 'float_lt1'],
    ['hueRotate', 'uint'],
    ['luminanceToAlpha', 'undefined']
];

var feComponentTransferTypes = [
    'feFuncA',
    'feFuncR',
    'feFuncB',
    'feFuncG'
];

var feCompositeTypes = [
    'over',
    'in',
    'out',
    'atop',
    'xor',
    'arithmetic'
];

var feConvolveMatrixTypes = [
    ['order', 'uint2'],
    ['divisor', 'int'],
    ['bias', 'floatlt1'],
    ['targetX', 'int'],
    ['targetY', 'int'],
    ['edgeMode', 'edgestr'], //"duplicate | wrap | none"
    ['kernelUnitLength', 'int2'],
    ['preserveAlpha', 'truefalse']
];

var feLightTypes = [
    'fePointLight', 
    'feDistantLight', 
    'feSpotLight'
];

var FilterResultIDs = new Array();


var genericNoFilterObject = new Filter("undefined", "undefined", false, "undefined");

var OneFilterForAll = new Filter("undefined", "undefined", false, "undefined");

var oneForAll = false;


Object.prototype.CreateShape = function()
{
    return this.createHTMLforShape();
};


/*
  Before invoking any functions one must call applyToObject to see if there exists a filter.
  If applyToObject returns false and you call any other function it's undefined, I guess, 
  I don't know javascript.
  */
function Filter(id_name, html, bool_apply){
    this.id_name = id_name;
    this.html = html;
    this.bool_apply = bool_apply;
}
Filter.prototype.getID = function() {
    return this.id_name;
}
Filter.prototype.getHTML = function() {
    return this.html;
}
Filter.prototype.applyToObject = function() {
    return this.bool_apply;
}


function generateTestCase(){
    var returnString = '<html> <title>SVG GEN</title><body><h1>SVG_GEN</h1>';
    

    //UINT_MAX = generateUInt(Math.floor(1000000));
    //console.log("UINT SIZE IS " + UINT_MAX);
    var height = generateUInt(Math.floor(2048));
    var width = generateUInt(Math.floor(2048));
    var svg = '<svg height="' + height + '" width="' + width + '">\n';
    
    var amount = generateUInt(200);
    
    for(var i = 0; i < amount ; i++){
	if((amount - i) % 10000 == 0){
	    console.log("Amount left: " + (amount - i));
	    global.gc();
	}
        svg += generateRandomSvg(height, width);
    }
    svg += '</svg>\n';

    returnString += svg;
    
    returnString += '</body></html>';

    if(OUTPUT == true){
        console.log(returnString + '\n\n');
    }
    return returnString;

}

function generateRandomSvg(height, width){
    var shuffled_objects = shuffle(objects);
    var returnString = "";
    
    /* TODO generate random amount of shapes */
    var fil = generateRandomFilter(height, width);
    /*if(fil.applyToObject() == true){
        returnString += fil.getHTML() + '\n';
    }*/
    if(oneForAll == false){
	returnString += fil.getHTML() + '\n';
	oneForAll = true;
    }
    var shape = new shuffled_objects[0][1](height, width, true, fil);
    returnString += shape.CreateShape() + '\n';
    

   // console.log(returnString);
    return returnString;
}

/*
  This will also create 'pathstrokes'
*/
function Path(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Path.prototype.createHTMLforShape = function() {
    var path_str = '<path d="M' + generateXYCoord(this.height, this.width) + ' ';
    var path_points = generateUInt(UINT_MAX);

    for(var i = 0; i < path_points; i++){
        path_str += generateRandomPathModifier() + generateXYCoord(this.height, this.width) + ' ';
    }
    path_str += 'Z" ';

    if(this.fil.applyToObject() == true){
        path_str += ' filter="url(#' + this.fil.getID() + ')" ';
     }

    if(this.do_styles = true){
        path_str += generateRandomStyle(this.height,this.width);
    }
    
    path_str += ' />';
    return path_str;
}


function Polyline(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Polyline.prototype.createHTMLforShape = function() {
    var polyl_str = '<polyline points="';
    var poly_points = generateUInt(UINT_MAX);
    for(var i = 0; i < poly_points; i++){
        polyl_str += generateXYCoord(this.width,this.height) + ' ';
    }
    polyl_str += '"';

    if(this.fil.applyToObject() == true){
        polyl_str += ' filter="url(#' + this.fil.getID() + ')" ';
    }
    if(this.do_styles == true){
        polyl_str += generateRandomStyle(this.width, this.height);
    }

    polyl_str += ' />';
    return polyl_str;
}



function Polygon(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Polygon.prototype.createHTMLforShape = function() {
    var poly_str = '<polygon points="';
    var poly_points = generateUInt(UINT_MAX);
    for(var i = 0; i < poly_points; i++){
        poly_str += generateXYCoord(this.width, this.height) + ' ';
    }
    poly_str += '"';

    if(this.fil.applyToObject() == true){
        poly_str += ' filter="url(#' + this.fil.getID() + ')" ';
     }

    if(this.do_styles == true){
        poly_str += generateRandomStyle(this.width, this.height);
    }

    poly_str += ' />';
    return poly_str;
}

function Line(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Line.prototype.createHTMLforShape = function() {
    var line_str = '<line x1="' + generateUInt(this.width) + '" y1="' + 
        generateUInt(this.height) + '" x2="' + generateUInt(this.width) +
        '" y2="' + generateUInt(this.height) + '" ';

     if(this.fil.applyToObject() == true){
        line_str += ' filter="url(#' + this.fil.getID() + ')" ';
     }

    if(this.do_styles == true){
        line_str += generateRandomStyle(this.width, this.height);
    }

    line_str += ' />';
    return line_str;
}


function Ellipse(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles
    this.fil = fil;
}
Ellipse.prototype.createHTMLforShape = function() {
    var elip_height = generateUInt(this.height);
    var elip_width = generateUInt(this.width);
    var elip_str = '<ellipse cx="' + elip_width + '" cy="' + elip_width + '"' +
        ' rx="' + generateUInt(elip_width) + '" ry="' + generateUInt(elip_height) + '" ';

    if(this.fil.applyToObject() == true){
        elip_str += ' filter="url(#' + this.fil.getID() + ')" ';
    }

    if(this.do_styles == true){
        elip_str += generateRandomStyle(this.width, this.height);
    }
    
    elip_str += ' />';
    return elip_str;
}

function Rectangle(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Rectangle.prototype.createHTMLforShape = function(){
    var rect_width = generateUInt(this.width);
    var rect_height = generateUInt(this.height);
    var rect_str = '<rect x="' + generateUInt(rect_width) + '" y="' +
        generateUInt(rect_height) + '" width="' + rect_width + '" ' +
        'height="' + rect_height + '" ';
    
    if(this.fil.applyToObject() == true){
        rect_str += ' filter="url(#' + this.fil.getID() + ')" ';
    }

    if(this.do_styles = true){
        rect_str += generateRandomStyle(this.width, this.height);
    }

    rect_str += " />"
    return rect_str;
}

function Circle(height, width, do_styles, fil){
    this.height = height;
    this.width = width;
    this.do_styles = do_styles;
    this.fil = fil;
}
Circle.prototype.createHTMLforShape = function(){
    var circ_str = '<circle cx="'+generateUInt(this.width)+'" cy="'+generateUInt(this.height)+'" r="'+generateUInt(Math.floor(Math.min(this.width,this.height)/2))+'" ';

    if(this.fil.applyToObject() == true){
        circ_str += ' filter="url(#' + this.fil.getID() + ')" ';
    }

    if(this.do_styles == true){
        circ_str += generateRandomStyle(this.width, this.height);
    }

    circ_str += ' />';
    return circ_str;
}


function generateRandomPathModifier(){
    if(generateUInt(4) % 3 != 0){
        return "";
    }
    var temp = path_modifiers[generateUInt(path_modifiers.length)][0];
    return temp;
}


//style="fill:rgb(etc);"   name(: (delimeter))value(; (end delim))
function generateRandomStyle(width, height){
    var t_style = shuffle(styles);
    var ret_str = 'style="';
    var length = generateUInt(t_style.length);
    
    for(var i = 0; i <= length; i++){
        ret_str += t_style[i][0] + ':';
        switch(t_style[i][1]){
        case "rgb":
            var rgb = generateRGB() + ';';
            ret_str += rgb;
            break;
        case "uint":
            ret_str += generateUInt(UINT_MAX) + ';';
            break;
        case "float_lt1":
            ret_str += generateFloatLT1() + ';';
            break;
        case "uint_px":
            ret_str += generateUInt(UINT_MAX) + 'px;';
            break;
        case "fill-string":
            ret_str += generateRandomFillRule() + ';';
            break;
        case "lcap-string":
            ret_str += generateRandomLineCap() + ';';
            break;
        case "Nxy-coord":
            var amount = generateUInt(UINT_MAX);
            while(--amount > 0){
                ret_str += generateXYCoord(width,height) + ',';
            }
            ret_str += generateXYCoord(width,height);
            ret_str += ';';
            break;
        case "ljoin-string":
            ret_str += generateRandomLineJoin() + ';';
            break;
        default:
            break;
        }
    }

    ret_str+='"';
    return ret_str;
}

function generateRandomFilter(height, width){
    /* Randomly determine if we're going to give a filter, If not, 
       return 'genericNoFilterObject
    */

  /*  if(generateFiftyProbability()){
        return genericNoFilterObject;
    }*/

    if(OneFilterForAll.applyToObject() != false){
	
	return OneFilterForAll;
    }

    var style_HTML = ' <filter id="';
    var id = generateRandomString(15);
 
    style_HTML += id + '" ';;
    style_HTML += 'x="' + generateUInt(width) + '" y="' + generateUInt(height) + '" width="' + generateUInt(300) + '%" ' +
        'height="' + generateUInt(300) + '%">\n';

    var html = "";

   
    if(DEBUG_ONE == true){
        html = recursiveFilterAdd("SourceGraphic", "",
                              1, filters.slice(), "",
                              height, width);
    }
    else{
        html = recursiveFilterAdd("SourceGraphic", "",
                                  generateUInt(filters.length), filters.slice(), "",
                                  height, width);
    }
    

    style_HTML += html +  '</filter>';
    

    var fil = new Filter(id,style_HTML,true);

    OneFilterForAll = fil;

    return fil;
}


/*    <feOffset result="offOut" in="SourceGraphic" dx="20" dy="20" />
      <feGaussianBlur result="blurOut" in="offOut" stdDeviation="10" />
      <feBlend in="SourceGraphic" in2="blurOut "mode="normal" /> */

function recursiveFilterAdd(_in, _in2, amount_left, available_filters, html, height, width){
    if(amount_left == 0){
        return html;
    }
    var result_str = generateRandomString(20);
    FilterResultIDs.push(result_str);
    var index = generateUInt(available_filters.length) 
    var filter_name =  available_filters[index];
    available_filters.splice(index,1); /* Remove the filter we just selected */
    amount_left--;
    
    /* randomly swap _in and _in2 if and only if this isn't the first iteration */
    
    if(_in2 != "" && (generateUInt(6) % 3) == 0){
        var temp;
        temp = _in2;
        _in2 = _in;
        _in = temp;
    
}
    html += '<' + filter_name + ' result="' + result_str + '" in="' + _in + '" ' +
        'in2="' + _in2 + '" ';

    switch(filter_name){
    case 'feBlend':
        html += 'dx="' + generateUInt(width) + '" dy="' + generateUInt(height) +
            '" />\n';
        break;
    case 'feColorMatrix':
        html += generateRandomfeColorMatrix() + '\n';
        break;
    case 'feComponentTransfer':
        html += '> ' + generateRandomfeComponentTransfer() + ' </feComponentTransfer>\n';
        break;
    case 'feComposite':
        html += generateRandomfeComposite() + '\n';
        break;
    case 'feConvolveMatrix':
        html += generateRandomfeConvolveMatrix() + '\n';
        break;
    case 'feDiffuseLighting':
        html += generateRandomDiffuseLighting(width,height) + '\n';
        break;
    case 'feDisplacementMap':
        html += generateRandomfeDisplacementMap() + '\n';
        break;
    case 'feFlood':
        html += generateRandomfeFlood(width,height) + '\n';
        break;
    case 'feGaussianBlur':
        html += generateRandomfeGaussianBlur() + '\n';
        break;
    case 'feImage':
        break;
    case 'feMerge':
        html += '>\n';
        html += generateRandomfeMerge() + '\n';
        break;
    case 'feMorphology':
        html += generateRandomfeMorphology() + '\n';
        break;
    case 'feOffset':
        html += generateRandomfeOffset(width,height) + '\n';
        break;
    case 'feSpecularLighting':
        html += generateRandomfeSpecularLighting(width, height);
        break;
    case 'feTile':
        html += '/>\n';
        break;
    case 'feTurbulence':
        html += generateRandomfeTurbulence() + '\n';
        break;
    case 'feDistantLight':
    case 'fePointLight':
    case 'feSpotLight':
        html += generatefeLightType(filter_name, width, height);
        break;
    default:
        console.log("default " + filter_name);
        break;
    }
   // console.log(html);
    return recursiveFilterAdd(result_str,_in,amount_left,available_filters, 
                              html, height, width);

}


function generateRandomfeTurbulence(){
    var ret_str = "";
    
    ret_str += 'baseFrequency="' + generateIntOrFloatLT1_neg(UINT_MAX);
    if(generateFiftyProbability()){
        ret_str += ' ' + generateIntOrFloatLT1_neg(UINT_MAX);
    }
    ret_str += '" numOctaves="' + generateInt(UINT_MAX) + '" ';
    ret_str += 'seed="' + generateIntOrFloatLT1_neg(UINT_MAX) + '" ';
    ret_str += 'stichTiles="' + shuffle(['stitch','noStitch'])[0] + '" ';
    ret_str += 'type="' + shuffle(['fractalNoise','turbulence'])[0] + '" />'
    
    return ret_str;
}

function generateRandomfeSpecularLighting(width, height){
    var ret_str = "";
    var attributes = shuffle( [
        ['suraceScale', 'floatorint'],
        ['specularConstant', 'floatorint'],
        ['specularExponent', 'floatorint'],
        ['kernelUintLength', '2int'],
        ['lighting-color', 'rgb']
    ] );

    var amount = generateUInt(attributes.length);
    
    for(var i = 0; i <= amount; i++){
        ret_str += attributes[i][0] + '="';
        switch(attributes[i][1]){
        case 'floatorint':
            ret_str += generateIntOrFloatLT1_neg(UINT_MAX) + '" ';
            break; 
        case '2int':
            ret_str += generateInt(UINT_MAX) + ' ' + generateInt(UINT_MAX) + '" ';
            break;
        case 'rgb':
            ret_str += generateRGB() + '" ';
            break;
        default:
            break;
        }
    }
    
    ret_str += ' >\n'

    var light_index = generateUInt(feLightTypes.length);
    
    ret_str += '<' + feLightTypes[light_index] + ' ';
    ret_str += generatefeLightType(feLightTypes[light_index], width, height);

    return ret_str + '</feSpecularLighting>\n';
}

function generateRandomfeOffset(width, height){
    var ret_str = "";
    ret_str += 'dx="' + generateInt(width) + '" dy="' + generateInt(height) +
        '" />';
    return ret_str;
}

function generateRandomfeMorphology(){
    var ret_str = "";
    ret_str += 'operator="' + shuffle(['erode' , 'dilate'])[0] + '" ';
    if(generateFiftyProbability()){
        ret_str += 'radius="' + generateUIntOrFloatLT1(UINT_MAX) + '" ';
    }

    ret_str += '/>'
    return ret_str;
}

function generateRandomfeMerge(){
    var num_mergeNodes = generateUInt(UINT_MAX);
    var ret_str = "";
    var num_ele = FilterResultIDs.length;
    for(var i = 0; i < num_mergeNodes; i++){
        if(i > 0 && (i % num_ele) == 0){
            shuffle(FilterResultIDs);
        }
        ret_str += '<feMergeNode in="' + FilterResultIDs[i % num_ele] + '"/>\n';
    }
    ret_str += '</feMerge>'
    return ret_str;
}

function generateRandomfeGaussianBlur(){
    var ret_str = "";
    //Half chance for No stddeviation
    if(generateFiftyProbability()){
        ret_str += ' stdDeviation="' + generateUIntOrFloatLT1(UINT_MAX);
        if(generateFiftyProbability()){
            ret_str += ' ' + generateUIntOrFloatLT1(UINT_MAX);
        }
        ret_str += '" />';
    }else{
        ret_str += ' />';
    }
    return ret_str;
}

function generateRandomfeFlood(width,height){
    var ret_str = "";

    ret_str += 'flood-color="' + generateRGB() + '" flood-opacity="' +
        generateIntOrFloatLT1_neg(UINT_MAX) + '" ';
    ret_str += (generateFiftyProbability()) ? '/>' : generateRandomStyle(width,height) + ' />';
    return ret_str;
}

function generateRandomfeDisplacementMap(){
    var ret_str = "";
    var rgba = ['R','G','B','A'];
    ret_str += ' scale="' + generateIntOrFloatLT1_neg(UINT_MAX) +
        '" xChannelSelector="' + shuffle(rgba)[0] + '" yChannelSelector="' +
        rgba[1] + '" />\n';
    return ret_str;
}

function generateRandomDiffuseLighting(width, height){
    var ret_str = "";
    var attributes = shuffle( [
        ['suraceScale', 'floatorint'],
        ['diffuseConstant', 'floatorint'],
        ['kernelUintLength', '2int'],
        ['lighting-color', 'rgb']
    ] );
                            


    var amount = generateUInt(attributes.length);
  //  console.log("AMOUNT IS " + amount + " AND attr Len is " + attributes.length);
    
    for(var i = 0; i <= amount; i++){
        ret_str += attributes[i][0] + '="';
        switch(attributes[i][1]){
        case 'floatorint':
            ret_str += generateIntOrFloatLT1_neg(UINT_MAX) + '" ';
            break; 
        case '2int':
            ret_str += generateInt(UINT_MAX) + ' ' + generateInt(UINT_MAX) + '" ';
            break;
        case 'rgb':
            ret_str += generateRGB() + '" ';
            break;
        default:
            break;
        }
    }
    
    ret_str += ' >\n'

    var light_index = generateUInt(feLightTypes.length);
    
    ret_str += '<' + feLightTypes[light_index] + ' ';
    ret_str += generatefeLightType(feLightTypes[light_index], width, height);

    return ret_str + '</feDiffuseLighting>\n';
}


function generatefeLightType(type, width, height){
    var ret_str = "";
    
    switch(type){
    case 'fePointLight':
        ret_str += 'x="' + generateUInt(width) + '" y="' + generateUInt(height) + '" ' +
            'z="' + generateUInt(UINT_MAX) + '" />\n';
        break;
    case 'feDistantLight':
        ret_str += 'azimuth="' + generateUInt(UINT_MAX) + '" elevation="' +
            generateUInt(UINT_MAX) + '" />\n';
        break;
    case 'feSpotLight':
        ret_str += 'x="' + generateUInt(width) + '" y="' + generateUInt(height) +
            '" z="' + generateUInt(UINT_MAX) + '" limitingConeAgle="' + 
            generateUInt(UINT_MAX) + '" pointsAtX="' + generateUInt(UINT_MAX) +
            '" pointsAtY="' + generateUInt(UINT_MAX) + '" pointsAtZ="' +
            generateUInt(UINT_MAX) + '" specularExponent="' + generateUInt(UINT_MAX) +
            '" />\n';
        break;
    default:
        break;
    }
   
    return ret_str;
}


function generateRandomfeConvolveMatrix(){
    var ret_str = "";
    var shuffledTypes = shuffle(feConvolveMatrixTypes);
    var amount = generateUInt(feConvolveMatrixTypes.length);


    var orderX = 1;
    var orderY = 1;

    for(var i = 0; i <= amount; i++){

        ret_str += shuffledTypes[i][0] + '="';
        
        switch(shuffledTypes[i][1]){
        case 'uint2':
            orderX = generateUInt(10);
            orderY = generateUInt(10);
            ret_str += orderX + ' ' + orderY + '" ';
            
            //Now setup the kernelMatrix
            ret_str += 'kernelMatrix="';
            var count = 0;
            if(generateUInt(6) % 5 != 0){
                count = orderX * orderY; //proper way to do it 
            }
            else{
                count = generateUInt(UINT_MAX); //not the correct way-- but we're fuzzing!
            }
            while(count > 0){
                ret_str += generateUInt(UINT_MAX) + ' ';
                count--;
            }
            ret_str += '" ';
            break;
        case 'int':
            ret_str += generateInt(UINT_MAX) + '" ';
            break;
        case 'edgestr':
            ret_str += generateRandomEdgeMode() + '" ';
            break;
        case 'floatlt1':
            ret_str += generateFloatLT1() + '" ';
            break;
        case 'int2':
            ret_str += generateInt(UINT_MAX) + ' ' + generateInt(UINT_MAX) + '" ';
            break;
        case 'truefalse':
            ret_str += generateTrueOrFalse() + '" ';
            break;
        default:
            break;
        }
    }
    return ret_str + ' />';
}

/*
Value 	over | in | out | atop | xor | arithmetic
https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/operator
*/
function generateRandomfeComposite(){
    var ret_str = "";
    var type = shuffle(feCompositeTypes)[0];
    switch(type){
    case 'over':
    case 'in':
    case 'out':
    case 'atop':
    case 'xor':
        ret_str += ' operator="' + type + '" />'; 
        break;
    case 'arithmetic':
        ret_str += ' operator="' + type + '"  k1="' + generateFloatLT1_neg() + '" k2="' +
            generateFloatLT1_neg() + '" k3="' + generateFloatLT1_neg() + '" k4="' +
            generateFloatLT1_neg() + '" />'; //closed out in upper switch
            
        break;
    default:
        break;
    }

    return ret_str;
}


/*
For the <fefuncr>, <fefuncg>, <fefuncb>, and <fefunca> elements
Value 	identity | table | discrete | linear | gamma
http://www.w3.org/TR/2003/REC-SVG11-20030114/filters.html
http://docs.webplatform.org/wiki/svg/elements/feComponentTransfer
*/
function generateRandomfeComponentTransfer(){
    var types = [
        'identity', 
        'table', 
        'discrete', 
        'linear',
        'gamma'
    ];
    
    var num_types = types.length-1;
    var num_feComTran = generateUInt(feComponentTransferTypes.length);

    var type_rand = shuffle(types);
    var feType_rand = shuffle(feComponentTransferTypes);
    
    var ret_str = "";
    
    for(var i = 0; i <= num_feComTran && num_types >= 0; i++){
        ret_str += '<' + feType_rand[i] + ' type="' + type_rand[num_types] + '" ';
        switch(type_rand[num_types]){
        case "identity":
            /* Nothing to do, identity is just a type and no params just break */
            ret_str += '/>\n';
            break;
            
        case "discrete":
            /*fall through*/
        case "table":
            ret_str += 'tableValues="';
            var x = generateUInt(UINT_MAX);
            while(x > 0){
                ret_str += generateFloatLT1() + ' ';
                x--;
            }
            ret_str += '"/>\n';
            break;
        case "linear":
            /* says it will take and "real number" Ints fall in real nums so lets throw those in too */
            var slope = generateIntOrFloatLT1_neg(UINT_MAX);
            var intercept = generateIntOrFloatLT1_neg(UINT_MAX);
            
            ret_str += 'slope="' + slope + '" intercept="' + intercept + '"/>\n';
                break;
        case "gamma":
            var amp = generateIntOrFloatLT1_neg(UINT_MAX);
            var expo = generateIntOrFloatLT1_neg(UINT_MAX);
            var off = generateIntOrFloatLT1_neg(UINT_MAX);
            ret_str += 'aplitutde="' + amp + '" exponent="' + expo + '" offset="' + off +
                '"/>\n';
            break;
        default:
            console.log("generateRandomfeComponent, this shouldnt happen, learn 2 code");
            break;
        }
        num_types--;
    }
    return ret_str;
}

function generateRandomfeColorMatrix(){
    var rand_index = generateUInt(feColorMatrixTypes.length);
    var type_str = 'type="';
    var values_str = 'values="';

    type_str += feColorMatrixTypes[rand_index][0] + '"';

    switch(feColorMatrixTypes[rand_index][1]){
    case "matrix":
        for(var i = 0; i < 20; i++){
            values_str += generateUInt(UINT_MAX) + ' ';
        }
        values_str += '" />';
        break;
    case "float_lt1":
        values_str += generateFloatLT1() + '" />';
        break;
    case "uint":
        values_str += generateUInt(UINT_MAX) + '" />';
        break;
    default:
        break;
    }
    return type_str + ' ' + values_str;
}


function generateRandomEdgeMode(){
    var edge_str = 
        ['duplicate',  'wrap',  'none'];
    return shuffle(edge_str)[0];
}

function generateRandomLineJoin(){
    var line_join =
        ['miter', 'round', 'bevel'];
    return shuffle(line_join)[0];
}

function generateRandomLineCap(){
    var line_cap = 
        ['butt', 'round', 'square'];
    return shuffle(line_cap)[0];
}

function generateRandomFillRule(){
    var fill_str = 
        ["nonzero", "evenodd",  "inherit"];
    return shuffle(fill_str)[0];
}


//
//Necessary exports
// 
module.exports={
 //
 //fuzz: 
 //Required synchronous function.
 //Must return a test case either as a Buffer or as a String	
 //
 fuzz:function(){
        return generateTestCase()
 },
 //
 //init:
 //Optional function
 //Doesn't require a return-value.
 //Can be used to do initialisations required by the fuzz-module.
 init:function(){
		config.type='text/html'
		config.tagtype='html'
		config.clientFile=config.reBuildClientFile()
 }
}