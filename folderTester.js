var fs=require('fs')

var count=0;
var folderContent=[]
var files=[]
var folderName=''
process.argv.forEach( function(arg,index){
if(arg.indexOf('foldername=')>-1){
	try{
		console.log(arg.split("=")[1])
		folderName=arg.split("=")[1]
		 var stats = fs.statSync(folderName);
      		if (stats.isDirectory()){
				folderContent=fs.readdirSync(folderName)
				for(var x in folderContent){
					if(!fs.statSync(folderName+'/'+folderContent[x]).isDirectory()){
						files.push(folderContent[x])
					}
				}
			}
			else{
				console.log('Error: '+folderName+' not a folder.')
			}
		
	}
	catch(e){
		console.log("Failed to get the file with error: "+e)
		process.exit(255)
	}
}
})


module.exports ={
	fuzz:function(){
		if(count<files.length){
			console.log('\n'+files[count])
			var fileContent=fs.readFileSync(folderName+files[count])
			count++
		}		
		else{
			count=0;
			console.log('\n'+files[count])
			var fileContent=fs.readFileSync(folderName+files[count])
			count++
		}
		return fileContent
	},
	init:function(){
		if(global.hasOwnProperty('config')){
			config.filetype='html'
			config.type='text/html; charset=utf-8;'
			config.tagtype='html'
			config.disableTestCaseBuffer=true
			config.clientTimeout=100
			config.timeout=20000
			config.testCasesWithoutRestart=5000
			config.clientFile=config.reBuildClientFile()
		}
		if(process.argv.indexOf('pdf')){
			console.log('pdf mode')
			config.type='application/pdf'
			config.filetype='pdf'
			
			config.clientFile=config.reBuildClientFile()
		}
	}
}