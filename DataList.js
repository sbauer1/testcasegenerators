/*
  Attempting to create a fuzzed version of:

    
<input list="test" name="fdsfdsfsdfds">
<datalist id="test">
  <option value="Internet Explorer">
  <option value="Firefox">
  <option value="Chrome">
  <option value="Opera">
  <option value="Safari">
</datalist>


*/

//Use getElementsByName()


function generateTestCase(){
    var returnString = '<html> <title>Test Title</title><body><h1>DATALIST_FUZZING</h1>';
    var input_string = '<input id="ilist" list="';
    var input_name = 'name="';
    var datalist = '<datalist id="';
    var temp;

    input_name+=generateRandomString(true);
 
    //Long string 
    temp = generateRandomString(true);
    input_string+= temp + '" ';
    input_string+=input_name + '">';
    datalist+=temp + '" name="dlist">';

    datalist+=generateRandomOptions(165);

    global.gc();
    datalist+="</datalist>";
    
    returnString+=input_string;    
    returnString+=datalist;
    returnString+='<script>';
    returnString+=generateJsDataListManipulation();
    returnString+='</script>';
    returnString+='</body></html>';
    //console.log(returnString);
    return returnString;

}

function generateRandomOptions(amount){
    var i;
    var datalist="";

    for(i = 0; i < Math.floor(Math.random() * amount); i++){
        datalist+='<option value="' + generateRandomString(true) + ' ">';
      
    }

    return datalist;
}


function generateJsDataListManipulation(){
    var script = 'window.onload=function(){\
var loop_max = Math.floor(Math.random() * 1000);\
 for(var i=0; i<loop_max; i++){\
                var datalist = document.getElementsByName("dlist")[0];\
                var elements = datalist.children;\
                var string = elements[Math.floor(Math.random() * (elements.length-1))].value;\
                var substr_size = Math.floor(Math.random() * string.length);\
                document.getElementById(\"ilist\").value = string.substring(0,substr_size);\
                if(Math.floor(Math.random()*2) % 2 == 1){\
                  datalist.removeChild(elements[Math.floor(Math.random() * (elements.length-1))]);\
                }\
                if(Math.floor(Math.random()*3) % 3 == 1){\
                  var max =  Math.floor(Math.random() * 10);\
                 for(var j=0; j < max; j++){\
                   var option = document.createElement(\"option\");\
                   option.value = generateRandomString();\
                   option.innerHTML = option.value;\
                   datalist.appendChild(option);\
                 }\
               }\
             }\
function generateRandomString(){\
    var text = \"\";\
    var possible = \"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\";\
    var length = 15 + Math.floor(Math.random() * 20);\
    for( var i=0; i < length ; i++ )\
        text += possible.charAt(Math.floor(Math.random() * possible.length));\
    return text;\
}\
}';
    
    return script;

}


// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}



function generateRandomString(short){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var length = Math.floor(Math.random() * 20)+1;//4294967295+1;
    if(short){
        lenght = Math.floor(Math.random() * 20)+1;
    }


    for( var i=0; i < length ; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));


    return text;
}


//
//Necessary exports
// 
module.exports={
 //
 //fuzz: 
 //Required synchronous function.
 //Must return a test case either as a Buffer or as a String	
 //
 fuzz:function(){
        return generateTestCase()
 },
 //
 //init:
 //Optional function
 //Doesn't require a return-value.
 //Can be used to do initlsialisations required by the fuzz-module.
 init:function(){
		config.type='text/html'
		config.tagtype='html'
		config.clientFile=config.reBuildClientFile()
 }
}